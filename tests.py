"""
Test that only entries return and content matches
"""
#from main import genre(), actor(), director()
from main import *


genre = genre()
actor = actor()
director = director()


def test_length_genre():
    assert len(genre) == 10


def test_length_actor():
    assert len(actor) == 10


def test_length_director():
    assert len(director) == 10


def test_genre():
    test = ['   Comedy',
 'Adventure',
 '   Family',
 '  Romance',
 '  Fantasy',
 ' Thriller',
 '   Action',
 '  Mystery',
 '    Music',
 'Biography'
    ]

    assert genre == test


def test_actor():
    test =['    Harrison Ford',
 '        Tom Hanks',
 '             Leon',
 'Jennifer Lawrence',
 '       Tom Cruise',
 ' Robert Pattinson',
 '   Bradley Cooper',
 '  Chris Hemsworth',
 '   Robin Williams',
 '     J.K. Simmons']

    assert actor == test


def test_director():
    test = {('profit', 'sum')}

    assert set(director) == test

