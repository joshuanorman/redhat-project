## RedHat Project for Data Engineer position  
This is a small python program to query a database from IMDb and return to the STDOUT a list of the most profitable directors, actors and genres from the database and also the heights of the most profitable actors (that last one was extra). The CSV that was provided was loaded into a local sqlite database and then data was loaded from that database into pandas dataframes.

Tested with python 3.9 on macOS, Fedora 34, Ubuntu 21.04
### Get Started  
To ensure dependencies are present
`pip install -r requirements.txt`
To initially create a sqlite database and load csv file. This is only necessary the first time or if you remove the databse.
`python3 setup.py or ./setup.py`
After setup the program may be executed as such:
`python3 main.py or ./main.py`
Unit tests may be run with:
`pytest tests.py`  

### To Do list
- UPDATE: actor() much faster now - no loop necessary
- Replace remaining for loops with faster solution for iteration over pandas data frames, likely something involving NumPy vectors.
- Properly modularize, separate functions to a separate file, remove print statements, etc.
