#!/usr/bin/env python3

import sqlite3
import pandas as pd
from imdb import IMDb


conn = sqlite3.connect('movies_db.db')

sql_query = pd.read_sql_query(
    '''select actor_1_name, actor_2_name, actor_3_name, director_name, genres, gross, budget, (gross - budget) as profit from movies_csv ''',
    conn)
df = pd.DataFrame(sql_query,
                  columns=['actor_1_name', 'actor_2_name', 'actor_3_name', 'director_name', 'genres', 'profit'])


def director():
    dir_new = df.groupby('director_name').agg({'profit': ['sum']})
    dir_new.sort_values(('profit', 'sum'), ascending=False, inplace=True)
    print("Most Profitable Directors:")
    for i in range(10):
        print('    ', dir_new.index[i])
    return dir_new.head(10)


def actor():
    #actor_merged = df.actor_1_name.map(str) + ',' + df.actor_2_name.map(str) + ',' + df.actor_3_name.map(str)
    #new_actor = pd.DataFrame(df.profit)
    #new_actor = new_actor.assign(actor=actor_merged)
    #actor_split = {j for i in new_actor.actor for j in i.split(',')}
    #single_actor, actor_profitability = [], []
    #for t in actor_split:
    #    c = new_actor.loc[new_actor.actor.str.match(t)].agg({'profit': ['sum']})
    #    single_actor.append(t)
    #    actor_profitability.append(float(c.profit))
    #df_actor = pd.DataFrame(data={'actor': single_actor, 'profitability': actor_profitability})
    #df_actor = df_actor.sort_values(by='profitability', ascending=False)
    #df_actor_formatted = df_actor.actor.head(10).to_string(index=False).splitlines()
    dfa1 = df[['actor_1_name','profit']]
    dfa2 = df[['actor_2_name','profit']]
    dfa3 = df[['actor_3_name','profit']]
    dfa_append = dfa1.append(dfa2).append(dfa3)
    dfa_append['actor'] = dfa_append.actor = dfa_append.actor_1_name.fillna(dfa_append.actor_2_name).fillna(dfa_append.actor_3_name)
    actor_merged = dfa_append.groupby('actor').agg({'profit': ['sum']})
    actor_merged.sort_values(('profit', 'sum'), ascending=False, inplace=True)
    actor_formatted = actor_merged.head(10).index
    def actor_profit():
        print("Most Profitable Actors:")
        for i in actor_formatted:
            print('    ', i.lstrip())

    actor_profit()

    def actor_imdb():
        ia = IMDb()
        print("Height of most profitable actors:")
        for i in actor_formatted:
            people = ia.search_person(i)
            people_id = people[0].personID
            people_info = ia.get_person(people_id)
            height = people_info['height']
            print('    ', i.lstrip(), ' is ', height, ' tall.')

    actor_imdb()

    return actor_formatted


def genre():
    genres = df.groupby('genres').agg({'profit': ['sum']})
    genres.sort_values(('profit', 'sum'), ascending=False, inplace=True)
    genre_split = {j for i in df.genres for j in i.split('|')}
    single_genre, profitability = [], []
    for t in genre_split:
        c = df.loc[df.genres.str.contains(t)].agg({'profit': ['sum']})
        single_genre.append(t)
        profitability.append(float(c.profit))
    df_genre = pd.DataFrame(data={'genre': single_genre, 'profitability': profitability})
    df_genre = df_genre.sort_values(by='profitability', ascending=False)
    df_genre_formatted = df_genre.genre.head(10).to_string(index=False).splitlines()
    print("Most Profitable Genres:")
    for i in df_genre_formatted:
        print('    ', i.lstrip())
    return df_genre_formatted

if __name__ == '__main__':
    director()
    genre()
    actor()
