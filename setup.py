#!/usr/bin/env python3

from pathlib import Path
import sqlite3
import pandas as pd

conn = sqlite3.connect('movies_db.db')

Path('movies_db.db').touch()
movies_csv = pd.read_csv('movie_metadata.csv')
movies_csv.to_sql('movies_csv', conn, if_exists='replace', index=False)
